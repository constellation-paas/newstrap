.ONESHELL: # This fake target make sure that all commands in a target are run in the same shell
SHELL := bash
.SHELLFLAGS = -ec
.DEFAULT_GOAL := help

TF_FILES := $(shell find terraform -name '*.tf')


.PHONY: build ## Build everything
build: packer-build docker-build

.PHONY: packer-build ## Build Packer images
packer-build: packer/output/manifest.json
packer/output/manifest.json: packer/node.pkr.hcl packer/provision/base/*
	packer build -force $<
	vagrant box add --force \
		local/constellation-ubuntu \
		packer/output/node/package.box

.PHONY: docker-build ## Build all docker images
docker-build: docker-bootstrap

.PHONY: docker-bootstrap ## Build the Docker image for the bootstrap
docker-bootstrap: docker/bootstrap.iid
docker/bootstrap.iid: docker/bootstrap.dockerfile src/stage-one $(TF_FILES)
	docker build                                    \
		-f docker/bootstrap.dockerfile          \
		-t local/constellation-bootstrap:latest \
		--iidfile $@                            \
		.

.PHONY: test t
t: test ## Alias for test
test:  test-stage-one test-units ## Run all the quick tests

watch-test: ## Watch files for changes, and trigger make test
	watchexec                       \
	    --on-busy-update=do-nothing \
	    --clear                     \
	    make test

tso: test-stage-one ## Alias for test-stage-one
test-stage-one: ## Test the first step of the bootstrap locally #quick
	@echo "🧪 Running stage one test"
	source env.sh || true
	export BATS_LIB_PATH="$(shell pwd)/vendors:$(shell pwd)/helpers"
	bats \
		--print-output-on-failure $(test_args) \
		--no-tempdir-cleanup \
		tests/stage-one

.PHONY: test-units tu
tu: test-units
test-units:
	@echo "🧪 Running unit tests"
	source env.sh || true
	export BATS_LIB_PATH="$(shell pwd)/vendors:$(shell pwd)/helpers"
	bats \
		--jobs $(shell nproc --all) \
		--print-output-on-failure $(test_args) \
		--no-tempdir-cleanup \
		tests/unit

.PHONY: deep-test dt
dt: deep-test ## Alias for deep-test
deep-test: test test-vagrant ## Run ALL the test

.PHONY: tv test-vagrant
tv: test-vagrant
test-vagrant: test-vagrant-ubuntu

.PHONY: test-vagrant-ubuntu tvu
vtu: vagrant-test-ubuntu ## Alias for test-vagrant-ubuntu
vagrant-test-ubuntu: build  ## Run the tests for the Vagrant Ubuntu platform
	cd tests/platform-vagrant-ubuntu
	mkdir -p constellation-init-data
	time vagrant up
	time vagrant ssh -- 'cd /opt/constellation-bootstrap && sudo ./platform/vagrant-ubuntu/bootstrap'

vru: vagrant-recreate-ubuntu ## Alias for vagrant-recreate-ubuntu
vagrant-recreate-ubuntu:
	cd tests/platform-vagrant-ubuntu
	vagrant destroy -f
	rm -rf constellation-init-data
	cd ../../
	make vagrant-test-ubuntu

vsu: vagrant-ssh-ubuntu ## Alias for vagrant-ssh-ubuntu
vagrant-ssh-ubuntu: ## Connect to the Ubuntu vagrant VM via SSH
	cd tests/platform-vagrant-ubuntu
	vagrant ssh

vdf: vagrant-destroy ## Alias for vagrant-destroy
vagrant-destroy: vagrant-destroy-ubuntu ## Force destruction of all vagrant VMs
vagrant-destroy-ubuntu:
	cd tests/platform-vagrant-ubuntu
	vagrant destroy -f

.PHONY: clean
clean: clean-tf  ## Clean the artifacts

.PHONY: clean-tf
clean-tf: ## Clean terraform artifacts
	find . -name '*.tfstate' -exec rm -rf {} \; || true
	find . -name '*.tfstate.backup' -exec rm -rf {} \; || true
	find . -name '.terraform' -exec rm -rf {} \; || true

clean-units-tests: clean-tf ## Clean stage one artifacts

clean-all: clean
	rm -rf docker/bootstrap.iid

.PHONY: help, h
h: help
help: ## This help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
