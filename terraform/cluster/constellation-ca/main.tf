variable "constellation_domain" {
  type        = string
  description = "The domain to use for the constellation cluster"
}

provider "vault" {}

module "pki_root" {
  source = "git::https://gitlab.com/constellation-paas/terraform-modules//modules/vault-ca?ref=bd506a2"
  #source   = "../../../../../terraform-modules/modules/vault-ca"
  pki_path = "constellation-ca"
  # 1 year of max lease
  pki_max_lease_ttl = "31536000"
  # This make the vault provider segfault
  #server_cert_domain = var.constellation_domain
  server_cert_domain = "constellation.test"
}
