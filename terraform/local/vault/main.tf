provisioner "file" {
  source = ""
}

variable "tls_disable" {
  description = "Configure if Vault will disable tls on the tcp listener"
  type        = string
  default     = "false"
}

variable "api_bind_adr" {
  description = "The bind address to use for the API"
  type        = string
  default     = "0.0.0.0"
}

variable "api_bind_port" {
  description = "The port to be used to bind the API"
  type        = number
  default     = 8200
}

variable "cluster_bind_adr" {
  description = "The bind address to use for the cluster communication"
  type        = string
  default     = "0.0.0.0"
}

variable "cluster_bind_port" {
  description = "The port to be used to bind the cluster"
  type        = number
  default     = 8201
}

resource "local_file" "foo" {
  content = templatefile(format("%s/config.hcl.tpl", path.module), {
    api_bind_adr      = var.api_bind_adr
    api_bind_port     = var.api_bind_port
    cluster_bind_adr  = var.cluster_bind_adr
    cluster_bind_port = var.cluster_bind_port
    tls_disable       = var.tls_disable
  })
  # TODO: parametrize output
  filename = format("%s/config.hcl", path.module)
}
