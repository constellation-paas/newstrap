# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/vault" {
  version     = "2.19.0"
  constraints = "~> 2.12"
  hashes = [
    "h1:6pygHfsbsb9Q0w/f8VmeMWx0WBsk8rQxTDmKMK7uyBQ=",
    "zh:3aa278b0ddb522270233673250fd01bf46422b995f85ab862e40264b68f72181",
    "zh:4fdc08743deb2c0c79ad8d459f6643fd0c75aac785c4a903103b35a98da1229a",
    "zh:55dad60c6d23a842db85fa79f6601f689cd761ae24256662923c5c5f7b735ce5",
    "zh:7e891e189773f9bb506048783c913207c73363117942a05101bfb1b9ab6e4634",
    "zh:852862a3868ceb3e52cf120d7c40c12b304847bbdfdd801f6fa367ae4926fe56",
    "zh:9844a25a2ad10f52e7c7898143be90c1fe7bba51c0fbbc05e7219629c08ac044",
    "zh:a4e42e987ae7c0d868b8848802b189a98b86133461f2b9ecd4ec0f798b855e6f",
    "zh:b1950043668e3a9654e7aaa8fa6de960e31f07a3546df4cd57d4ff50e247f1ad",
    "zh:faae594a8683d58070ec4362fa606e59518052823c51065f28c4013b674d2976",
    "zh:fd0cc6a461c144ae4b6c427be84bbafa057a6a7906fc3b50a972148d0e6072b4",
  ]
}
