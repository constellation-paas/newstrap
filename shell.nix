with (import <nixpkgs> {});
let
  unstable = (import <unstable> {});
in
  mkShell {
    buildInputs = [
      gnumake
      coreutils
      findutils
      bash
      unstable.terraform
      unstable.packer
      unstable.shellcheck
      unstable.shfmt
      nomad
      consul
      vault
      bats
    ];
  }
