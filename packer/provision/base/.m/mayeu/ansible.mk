include .m/mayeu/python.mk

$(info, "Python deps path: $(PYTHON_DEPS)")

ANSIBLE_DEPS ?= roles_vendor collections_vendor

ansible-playbook = ansible-playbook $(ANSIBLE_OPTS) $(ansible_additional_args)

deps-ansible: $(ANSIBLE_DEPS) ## Install ansible deps

roles_vendor: requirements.yml $(PYTHON_DEPS)
	$(python_activate_venv)
	ansible-galaxy install --force -r requirements.yml
	touch $@
	deactivate

collections_vendor: requirements.yml $(PYTHON_DEPS)
	$(python_activate_venv)
	ansible-galaxy collection install --force -r requirements.yml
	touch $@
	deactivate

ansible-clean:
	rm -rf $(ANSIBLE_DEPS)
