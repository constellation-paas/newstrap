PYTHON_VERSION ?= python3.8
VENV_PATH = .venv-$(PYTHON_VERSION)
PYTHON_DEPS = $(VENV_PATH)/lib/$(PYHON_VERSION)/site-packages

python_activate_venv = source $(VENV_PATH)/bin/activate

$(VENV_PATH):  ## Create the python virtualenv
	mkdir $@
	virtualenv -p "$(PYTHON_VERSION)" $@

$(PYTHON_DEPS): $(VENV_PATH) requirements.txt
	$(python_activate_venv)
	pip install -r requirements.txt
	# We have to touch the folder because it could happen that the folder
	# time don't get updated even after new installation.
	touch $@
	deactivate

venv-shell: $(PYTHON_DEPS) ## This start a shell in the venv environement
	$(activate_venv)
	$(SHELL)
	deactivate

python-clean:
	rm -rf $(VENV_PATH)
