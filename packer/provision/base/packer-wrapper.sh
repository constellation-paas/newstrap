#! /usr/bin/env bash
set -euo pipefail

PYTHON_VERSION=${PYTHON_VERSION:-"python3.8"}

# Execution is relative to our project root, so we have to go back in this dir
# Not really ideal
cd packer/provision/base

make deps

source .venv-${PYTHON_VERSION}/bin/activate

ANSIBLE_FORCE_COLOR=1 PYTHONUNBUFFERED=1 ansible-playbook "$@"

deactivate
