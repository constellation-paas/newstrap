variable "output_path" {
  type    = string
  default = "./packer/output/"
}

source "vagrant" "node" {
  communicator = "ssh"
  source_path  = "ubuntu/focal64"
  provider     = "virtualbox"
  add_force    = true
  output_dir   = "${var.output_path}/node"
}

build {
  sources = ["source.vagrant.node"]

  provisioner "ansible" {
    # Relative to where we launch packer
    command       = "./packer/provision/base/packer-wrapper.sh"
    playbook_file = "./packer/provision/base/provision.yml"
  }

  post-processor "manifest" {
    output     = "./${var.output_path}/manifest.json"
    strip_path = true
  }
}
