#!/usr/bin/env bats

bats_load_library 'bats-support'
bats_load_library 'bats-assert'
#bats_load_library 'bats-terraform'

setup() {
    # We source the default env and then the test one
    source "${BATS_TEST_DIRNAME}/../../env.sh"
    source "${BATS_TEST_DIRNAME}/stage-one-env.sh"

    # Store anything terraform creates in an isolated folder unique to the currently running test
    export TF_DATA_DIR="${BATS_TEST_TMPDIR}/.terraform"
    export TF_CLI_ARGS_init="-backend-config='path=${BATS_TEST_TMPDIR}/terraform.tfstate' -backend-config='workspace_dir=${BATS_TEST_TMPDIR}/terraform.tfstate.d/'"
    mkdir -p "${CONSTELLATION_VAULT_ETC}"
    cp "${BATS_TEST_DIRNAME}/../../src/vault/bootstrap-stage-one-config.hcl" \
        "${CONSTELLATION_VAULT_ETC}/config.hcl"
}

teardown() {
    docker stop "${CONSTELLATION_VAULT_CONTAINER_NAME}" || true
    docker rm "${CONSTELLATION_VAULT_CONTAINER_NAME}" || true
}

@test "stage-one should complete successfully with valid value" {
    # Run stage-one
    run "${BATS_TEST_DIRNAME}/../../src/stage-one"

    assert_success
}
