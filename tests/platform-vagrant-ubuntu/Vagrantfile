# -*- mode: ruby -*-
# vi: set ft=ruby :

$vm_gui = false
$vm_memory = 4096
$vm_cpus = 4
$vm_ip = "172.17.8.8"
$shared_folders = {"../../" => "/opt/constellation-bootstrap", "constellation-init-data" => "/constellation/init-data"}
$forwarded_ports = {4646 => 4646, 443 => 8443, 8888 => 8888, 8080 => 8080, 8500 => 8500, 8200 => 8200 }

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "local/constellation-ubuntu"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # from our machine
  #
  # In the example below, accessing "localhost:8080" will access port 80 on
  # the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  config.vm.network :private_network, ip: $vm_ip
 
  $forwarded_ports.each do |guest, host|
    config.vm.network "forwarded_port", guest: guest, host: host, auto_correct: true
  end

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  $shared_folders.each_with_index do |(host_folder, guest_folder), index|
        config.vm.synced_folder host_folder.to_s,
                                guest_folder.to_s,
                                id: "vagrant-share%02d" % index,
                                nfs: true,
                                mount_options: ['nolock,vers=3,udp']
  end


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    vb.memory = $vm_memory
    vb.cpus = $vm_cpus

    # Display the VirtualBox GUI when booting the machine
    vb.gui = $vm_gui
  end
  
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
