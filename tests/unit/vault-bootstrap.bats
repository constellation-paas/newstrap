#!/usr/bin/env bats

bats_load_library 'bats-support'
bats_load_library 'bats-assert'

## General options tests

setup() {
    source "${BATS_TEST_DIRNAME}/../../src/lib/vault-bootstrap.sh"
    source "${BATS_TEST_DIRNAME}/../../env.sh"
    source "${BATS_TEST_DIRNAME}/vault-bootstrap.env.sh"

    mkdir -p "${CONSTELLATION_VAULT_ETC}"
    cp "${BATS_TEST_DIRNAME}/../../src/vault/bootstrap-stage-one-config.hcl" \
        "${CONSTELLATION_VAULT_ETC}/config.hcl"
}

teardown() {
    docker stop "${CONSTELLATION_VAULT_CONTAINER_NAME}" || true
    docker rm "${CONSTELLATION_VAULT_CONTAINER_NAME}" || true
}

@test "wait_for_vault() should timeout and fail after the given time" {
    run wait_for_vault 2

    assert_failure
}

@test "The vault docker image should start with the default bootstrap config" {
    run start_vault '127.0.0.1' "$CONSTELLATION_VAULT_CONTAINER_NAME"

    assert_success

    wait_for_vault

    assert_success
}

#@test "A fake arg should be rejected with an error and print the usage" {
#    run $bin --lol-this-is-fake
#
#    [ "$status" -eq 1 ]
#    echo "$output" | grep -q "Error"
#    echo "$output" | grep -q "Usage"
#}
