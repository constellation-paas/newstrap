#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

# Constellation configuration

export CONSTELLATION_DOMAIN=constellation.test
export CONSTELLATION_INIT=/constellation/init-data

export CONSTELLATION_BOOTSTRAP_EXPECT=1

export CONSTELLATION_NOMAD_SERVER_ETC="/etc/nomad-server"
export CONSTELLATION_NOMAD_CLIENT_ETC="/etc/nomad-client"
export CONSTELLATION_NOMAD_VERSION=
export CONSTELLATION_NOMAD_DC=dc1 # This should not be nomad specific, it's also used by consul
export CONSTELLATION_NOMAD_REGION=region1
export NOMAD_LOCAL_CONFIG='{ "disable_update_check": true, "ports": { "http": 5454 }}'

export CONSTELLATION_CONSUL_VERSION=1.9.5
export CONSTELLATION_CONSUL_ETC="/etc/consul"
export CONSTELLATION_CONSUL_BIND=eth0
export CONSUL_LOCAL_CONFIG='{ "disable_update_check": true}'

export CONSTELLATION_VAULT_VERSION=1.11.3
export VAULT_LOCAL_CONFIG='{"storage": {"raft": {"node_id": "%H", "path": "/vault/file"}}}'
export CONSTELLATION_VAULT_DATA="/constellation/system/vault-server/data"
export CONSTELLATION_VAULT_ETC="/constellation/system/vault-server/etc"
export CONSTELLATION_VAULT_PKI_PATH="constellation-ca"

export CONSTELLATION_CONSUL_TEMPLATE_ETC="/etc/consul-template"

export TERRAGRUNT_URL="https://github.com/gruntwork-io/terragrunt/releases/download/v0.23.32/terragrunt_linux_amd64"

# Bootstrap specific
export VAULT_ADDR="http://127.0.0.1:8200"
export GPG_KEYSERVER="keys.openpgp.org"
export GPG_FINGERPRINT="A016F2D8047211861B33A41922B10496B00DA693"
export CONSTELLATION_GENERATE_CA=true

# Constellation dev mode (deactivated by default)
export CONSTELLATION_DEV_MODE=false
export VAULT_DEV_MODE_TOKEN=root-token
