# TODO: parametrize the vault version
FROM vault:1.7.1 as vault

# TODO: parametrize the terraform version
FROM hashicorp/terraform:0.15.3

RUN apk add --update --no-cache \
            jq \
            bash \
            curl \
            gnupg \
            ca-certificates \
            dumb-init \
            docker

COPY --from=vault /bin/vault /bin/vault

RUN mkdir /bootstrap

WORKDIR /bootstrap

COPY src/  src/
COPY terraform/  terraform/

RUN chmod +x src/stage-one

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD src/stage-one
