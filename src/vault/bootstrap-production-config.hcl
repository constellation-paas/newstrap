# This is the initial bootstrap configuration for Vault. It should not be used
# for production setup, and should be replaced by one generated during the
# bootstrap phase.
listener "tcp" {
   address            = "0.0.0.0:8200"
   cluster_address    = "0.0.0.0:8201"
   tls_disable        = false
   tls_cert_file      = "/vault/config/tls/vault-server.crt"
   tls_key_file       = "/vault/config/tls/vault.key"
   tls_min_version    = "tls13"
   tls_client_ca_file = "/constellation/system/vault-server/etc/tls/constellation-ca-chain.crt"
 }

# TODO: this addresses should be dynamicly set depending on the setup currently deployed
# TODO: Providable by VAULT_API_ADDR & VAULT_CLUSTER_ADDR
# TODO: VAULT_API_ADDR this should basically be the LB of the Vault cluster or the domain pointing to the Vault master
cluster_addr = "https://172.17.8.8:8201"
api_addr = "https://172.17.8.8:8200"

default_lease_ttl = "168h"
max_lease_ttl = "720h"
