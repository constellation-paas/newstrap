# This is the initial bootstrap configuration for Vault. It should not be
# used for production setup, and should be replaced by one generated during
# the bootstrap phase.

# We use the intgrated storage, so we disable mlock as highlight in the
# official documentation:
# https://www.vaultproject.io/docs/configuration#disable_mlock
disable_mlock = true

storage "raft" {
  path = "/vault/file"
}


listener "tcp" {
  address         = "0.0.0.0:8200"
  cluster_address = "0.0.0.0:8201"
  tls_disable     = true
}

cluster_addr = "http://127.0.0.1:8201"
api_addr     = "http://127.0.0.1:8200"

default_lease_ttl = "168h"
max_lease_ttl     = "720h"
