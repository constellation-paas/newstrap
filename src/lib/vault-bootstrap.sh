#! /usr/bin/env bash

info() {
  local entry=${1:-}

  echo "INFO: ${entry}" >&2
}

fatal() {
  local entry=${1:-}

  echo "FATAL: ${entry}" >&2
  exit 1
}

start_vault() {
  local network_interface="${1:-127.0.0.1}"
  local name=${2:-"constellation-vault-bootstrap"}
  # TODO: Make the dev mode configurable via a CONSTELLATION_DEV_MODE env variable.
  # Or make it easy to export the init to dev with a closer to reality setup

  docker run \
    --name "$name" \
    --detach \
    --cap-add=IPC_LOCK \
    -p "${network_interface}:${CONSTELLATION_VAULT_CONTAINER_API_PORT}:8200" \
    -p "${network_interface}:${CONSTELLATION_VAULT_CONTAINER_CLUSTER_PORT}:8201" \
    -v "${CONSTELLATION_VAULT_DATA}:/vault/file" \
    -v "${CONSTELLATION_VAULT_ETC}:/vault/config" \
    -e "VAULT_RAFT_NODE_ID=$(hostname)" \
    vault:${CONSTELLATION_VAULT_VERSION} server \
    -log-level="trace"

  sleep 1
}

wait_for_vault() {
  local timeout=${1:-30}

  local start_time=$(date +"%s")
  local seal_status="$VAULT_ADDR/v1/sys/seal-status"
  local check="curl --output /dev/null --silent --fail '$seal_status'"
  local timed_out=0

  until test "$timed_out" -eq "1" || curl --output /dev/null --silent --fail "$seal_status"; do
    builtin printf '.'
    sleep 1
    if [[ "(( $(date +"%s") - $start_time ))" -gt "$timeout" ]]; then
      timed_out=1
    fi
  done

  if [[ "$timed_out" -eq "1" ]]; then
    fatal "Waited for Vault for ${timeout}s already, hanging up"
  fi

  return
}

initialise_vault() {
  # TODO: Support the key shares and key threshold arguments
  vault operator init -format=json
}

encrypt_initialisation_on_disk() {
  declare init="${1:-}"

  mkdir -p "$CONSTELLATION_INIT"

  gpg --keyserver "$GPG_KEYSERVER" --receive-keys "$GPG_FINGERPRINT"
  echo "$init" |
    gpg --trust-model always \
      --armor \
      --encrypt \
      --recipient "$GPG_FINGERPRINT" >"$CONSTELLATION_INIT/vault-init.asc"

  # TODO: Remove that and make the GPG optional via an env variable
  echo "$init" >"$CONSTELLATION_INIT/vault-init-unsecure.json"
}

break_vault_seal() {
  declare keys="${1:-}"

  echo "$keys" |
    head -n3 |
    xargs -I{} -n1 vault operator unseal {} >/dev/null

  info "Waiting for Vault to unseal..."

  sleep 15
  until vault status >/dev/null; do
    builtin printf '.'
    sleep 1
  done
}

generate_cert_for() {
  # TODO 2020-09-30: this should test for the existence of the env variable
  # instead of setting a default.
  # COMMENT 2021-03-25: I have no idea what that todo means :thinking: Maybe
  # because the env variable can be empty and thus mess things up?
  local name="${1:-""}"
  local tlsdir="${2:-""}"

  mkdir -p "$tlsdir"

  local certificate_data
  certificate_data="$(vault write -format=json \
    "${CONSTELLATION_VAULT_PKI_PATH}-intermediate/issue/server-certificate" \
    common_name="${name}.${CONSTELLATION_DOMAIN}")"

  # TODO 2020-09-30: The consul-template part of the name should actually be the whole domain
  echo "$certificate_data" | jq -r '.data.private_key' >"${tlsdir}/${name}.key"
  echo "$certificate_data" | jq -r '.data.certificate' >"${tlsdir}/${name}.crt"
  echo "$certificate_data" | jq -r '.data.ca_chain | .[]' >"${tlsdir}/constellation-ca-chain.crt"

  echo "$certificate_data" | jq -r '"\(.data.certificate)\n\(.data.ca_chain|.[])"' >"${tlsdir}/constellation-certificate-ca-chain.crt"
}

extract_unseal_keys() {
  local json="${1:-""}"

  echo "$json" | jq -r '.unseal_keys_b64 | .[]'
}

bootstrap_vault() {
  info "Waiting for Vault to show up..."
  wait_for_vault

  # Vault initialisation
  info "Running the initialisation of Vault..."
  readonly init_response="$(initialise_vault)"

  readonly unseal_keys="$(extract_unseal_keys "$init_response")"
  readonly vault_token=$(echo -n "$init_response" | jq -r .root_token)

  # Save the initialisation
  encrypt_initialisation_on_disk "$init_response"

  # Unseal Vault
  info "Breaking Vault's seal..."
  break_vault_seal "$unseal_keys"
  info "Vault is unsealed"

  echo -n "$vault_token"
}

generate_vault_production_configuration() {
  # 2021-05-30 TODO: this will be replaced by a template in TF
  cp "$(dirname "$0")/vault/bootstrap-production-config.hcl" "${CONSTELLATION_VAULT_ETC}/config.hcl"
}
